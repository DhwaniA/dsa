package recursion.io;

public class PermutationWithSpaces {
    static void solve(String ip, String op){
        if(ip.length() == 0){
            System.out.println(op);
            return;
        }
        // make choices
        String op1 = op + ip.charAt(0);
        String op2 = op + ip.charAt(0) + "_";
        //smaller input
        ip = ip.length() == 1 ? "" : ip.substring(1);

        solve(ip, op1);
        if(ip.length() != 0) // since we don't wish to consider spaces for the last character
            solve(ip, op2);
    }

    public static void main(String[] args) {
        solve("ABC", "");
    }
}
