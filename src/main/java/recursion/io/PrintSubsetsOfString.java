package recursion.io;

/*
Given a string print all possible substrings
Synonyms: print all subset, print power set, print all subsequences'
substring - continuous
subsequence - can be non-continuous but order matters. Eg "abc" => "ac" is valid but "ca" is not
subset - con be non-continuous and order doesn't matter
 */
public class PrintSubsetsOfString {
    static void solve(String input, String output){
        //base condition
        if (input.length() == 0){
            System.out.println(output);
            return;
        }

        String output1 = output;
        String output2 = output + input.charAt(0);

        //make input smaller
        input = input.length() == 1 ? "" : input.substring(1);

        solve(input, output1);
        solve(input, output2);
    }

    /*
    Print unique subsets = same solution but store you results in a set to get unique values
     */
    public static void main(String[] args) {
        solve("abc","");
    }
}
