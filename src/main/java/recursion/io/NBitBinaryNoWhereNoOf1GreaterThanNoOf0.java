package recursion.io;

import java.util.ArrayList;

public class NBitBinaryNoWhereNoOf1GreaterThanNoOf0 {
    static ArrayList<String> NBitBinary(int N) {
        ArrayList<String> result = new ArrayList<>();
        solve(0, 1, N-1, "1", result);
        return result;
    }

    static void solve(int numOfZeroes, int numOfOnes, int n, String op, ArrayList<String> result){
        if(n == 0){
            result.add(op);
            return;
        }

        if(numOfOnes + 1 >= numOfZeroes){
            solve(numOfZeroes, numOfOnes + 1, n-1, op + "1", result);
        }

        if(numOfOnes >= numOfZeroes + 1){
            solve(numOfZeroes +1, numOfOnes, n-1, op + "0", result);
        }
    }

    public static void main(String[] args) {
        NBitBinary(3).stream().forEach(System.out::println);
    }
}
