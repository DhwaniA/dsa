package recursion.io;

public class GenerateBalancedParantheses {
    static void solve(int numOfOpenBraces, int numOfClosedBraces, String op){
        // base condition
        if (numOfOpenBraces == 0 && numOfClosedBraces == 0){
            System.out.println(op);
            return;
        }

        // make the input smaller
        if(numOfOpenBraces >= 0) {
            String op1 = op + "(";
            solve(numOfOpenBraces - 1, numOfClosedBraces, op1);
        }

        if(numOfClosedBraces > numOfOpenBraces){
            String op2 = op + ")";
            solve(numOfOpenBraces, numOfClosedBraces -1, op2);
        }
    }

    public static void main(String[] args) {
        solve(2,3,"(");
    }
}
