package recursion.io;

// Letter Case Permutation
public class PermutationWithCaseChange {
    public static void solve(String ip , String op){
        if(ip.length() == 0){
            System.out.println(op);
            return;
        }

        if(Character.isAlphabetic(ip.charAt(0))){
            solve(ip.substring(1), op + Character.toLowerCase(ip.charAt(0)));
            solve(ip.substring(1), op + Character.toUpperCase(ip.charAt(0)));
        } else
            solve(ip.substring(1), op + ip.substring(0,1));
    }

    public static void main(String[] args) {
        solve("ab","");
    }
}
