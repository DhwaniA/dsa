package recursion;

import java.util.ArrayList;
import java.util.List;

public class BinaryStringWithNoConsOnes {
    static List< String > generateString(int N) {
        // Write your code here.
        List<String> result = new ArrayList<>();
        solve(N, "", result);
        return result;
    }
    static void solve(int n, String op, List<String> result){
        if(n == 0) {
            result.add(op);
            return;
        }

        solve(n-1, op + "0", result);
        if( op.length() == 0 || op.substring(op.length()-1).equals("0")){
            solve(n-1, op + "1", result);
        }
    }

    public static void main(String[] args) {
        generateString(2).stream().forEach(System.out::println);
    }
}
