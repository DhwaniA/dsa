package recursion.ibh;

public class TowerOfHanoi {
    static void solve(int n, char source, char destination, char helper){
        //base condition
        if(n == 1){
            System.out.println("Move plate " + n + " from " + source + " to " + destination);
            return;
        }

        solve(n-1, source, helper, destination);
        System.out.println("Move plate " + n + " from " + source + " to " + destination);
        solve(n-1, helper, destination, source);
    }

    public static void main(String[] args) {
        solve(3, 'S', 'D', 'H');
    }
}
