package recursion.ibh;

import java.util.Stack;

public class SortStack {
    public static void sortStack(Stack<Integer> stack) {
        // base condition
        if(stack.size() == 1){
            return;
        }
        // hypothesis
        Integer topElement = stack.pop();
        sortStack(stack);
        // induction
        insertElement(stack, topElement);
    }

    // Assume that the stack provided is already sorted
    static void insertElement( Stack<Integer> stack, Integer num ){
        // base condition
        if(stack.isEmpty() || stack.peek() <= num ){
            stack.push(num);
            return;
        }

        Integer topElement = stack.pop();
        insertElement(stack, num);

        stack.push(topElement);
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(1);
        stack.push(0);
        stack.push(2);

        sortStack(stack);

        System.out.println(stack);
    }
}
