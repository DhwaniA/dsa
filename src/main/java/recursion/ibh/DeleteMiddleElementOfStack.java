package recursion.ibh;

import java.util.Stack;

public class DeleteMiddleElementOfStack {
    // middle element position = (size/2) + 1
    static void deleteElement(Stack<Integer> stack, int position) {
        // base condition
        if (position == 1){
            stack.pop();
            return;
        }

        int topElement = stack.pop();
        deleteElement(stack, position - 1);
        stack.push(topElement);
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(1);
        stack.push(0);
        stack.push(2);

        int middlePosition = (stack.size()/2) + 1 ;
        deleteElement(stack,middlePosition);

        System.out.println(stack);
    }
}
