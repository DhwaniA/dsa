package recursion.ibh;

import java.util.Stack;

public class ReverseStack {
    static void reverseStack(Stack<Integer> stack) {
        // base condition
        if(stack.size() == 1){
            return;
        }

        //hypothesis = smaller Input
        int topElement = stack.pop();
        reverseStack(stack);

        // induction = insert the popped element at last
        insert(stack, topElement);
    }

    private static void insert(Stack<Integer> stack, int num) {
        // base condition
        if( stack.size() == 0){
            stack.push(num);
            return;
        }

        // hypothesis = smaller input
        int topElement = stack.pop();
        insert(stack,num);

        // induction = add the popped element back to the stack
        stack.push(topElement);
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(5);
        stack.push(1);
        stack.push(0);
        stack.push(2);

        reverseStack(stack);

        System.out.println(stack);
    }
}
