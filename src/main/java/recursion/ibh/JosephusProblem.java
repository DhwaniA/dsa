package recursion.ibh;

import java.util.ArrayList;
import java.util.List;

public class JosephusProblem {
    static int findPosition(int n, int k){
        List<Integer> peopleStanding = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            peopleStanding.add(i, i+1);
        }
        // o based indexing
        solve(0, k-1,peopleStanding);
        return peopleStanding.get(0);
    }
    static void solve(int currentPosition, int k, List<Integer> peopleStanding){
        if(peopleStanding.size() == 1){
            return;
        }
        int nextKill = (currentPosition + k) % peopleStanding.size();
        peopleStanding.remove(nextKill);
        solve(nextKill, k, peopleStanding);
    }

    public static void main(String[] args) {
        System.out.println(findPosition(40,7));
    }
}
