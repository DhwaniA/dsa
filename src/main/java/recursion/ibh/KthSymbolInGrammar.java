package recursion.ibh;

public class KthSymbolInGrammar {
    public static int kthGrammar(int n, int k) {
        // base condition
        if( n == 1 && k == 1){
            return 0;
        }

        // hypothesis = smaller input
        int lengthOfRow = (int) Math.pow(2, n-1);
        int mid = lengthOfRow/2;

        if( k <= mid )
            return kthGrammar(n-1, k);
        else
            return kthGrammar(n-1, k-mid) == 0 ? 1 : 0;
    }

    public static void main(String[] args) {
        System.out.println(kthGrammar(2,2));
    }
}
