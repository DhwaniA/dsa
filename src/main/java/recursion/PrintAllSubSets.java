package recursion;

import java.util.ArrayList;
import java.util.List;

public class PrintAllSubSets {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        solve(0,nums, new ArrayList<>(), result);
        return result;
    }

    static void solve(int position, int[] nums, List<Integer> output, List<List<Integer>> result){
        if(position == nums.length){
            result.add(output);
            return;
        }

        List<Integer> output1 = new ArrayList<>(output);
        output1.add(nums[position]);
        solve(position+1,nums,output1,result);
        solve(position+1,nums,new ArrayList<>(output),result);
    }
}
