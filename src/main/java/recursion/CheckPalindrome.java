package recursion;

public class CheckPalindrome {
    public static boolean isPalindrome(String str) {
        // Write your code here.
        int start = 0;
        int end = str.length();
        if(start < end){
            if(str.charAt(start) != str.charAt(end)){
                return false;
            } else {
                return isPalindrome(str.substring(1, str.length() -1));
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome("abbba"));
    }
}
