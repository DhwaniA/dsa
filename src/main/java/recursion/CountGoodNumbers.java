package recursion;

public class CountGoodNumbers {
    // this is causing submission issue - logic seems okay
    static int solve( long numOfDigits){
        if(numOfDigits == 1){
            return 5;
        }

        if((numOfDigits - 1) % 2 == 0){
            return (5 * solve(numOfDigits-1))%(1_000_000_007);
        } else {
            return (4 * solve(numOfDigits-1))%(1_000_000_007);
        }
    }

    public static void main(String[] args) {
        System.out.println(solve(4));
    }
}
