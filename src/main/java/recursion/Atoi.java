package recursion;

import java.util.ArrayList;

public class Atoi {
    static int atoi(String s){
        String[] result = new String[1];
        s = s.trim();
        if(s.length() > 1 && s.charAt(0) == '+' || s.charAt(0) == '-'){
            solve(s.substring(1), "",result);
            try {
                return Integer.parseInt(s.charAt(0) + result[0]);
            } catch (NumberFormatException e){
                if(s.charAt(0) == '+')
                    return Integer.MAX_VALUE;
                else
                    return Integer.MIN_VALUE;
            }
        } else {
            solve(s, "",result);
            try {
                return result[0].length() == 0 ? 0 : Integer.parseInt(result[0]);
            } catch (NumberFormatException e){
                return Integer.MAX_VALUE;
            }
        }
    }
    static void solve(String ip, String op, String[] result){
        if(ip.length() == 0 || !Character.isDigit(ip.charAt(0))){
            result[0] = op;
            return;
        }
        String op1 = op + ip.charAt(0);
        ip = ip.length() == 1 ? "" : ip.substring(1);
        solve(ip , op1, result);
    }

    // normal solution
//    static int findAtoi(String s){
//        s = s.trim();
//        if(s.length() == 0)
//            return 0;
//
//        char first = s.charAt(0);
//        String output = "";
//        for (int i = 0; i < s.length(); i++) {
////            if(i == 0 &&)
//            if(!Character.isDigit(s.charAt(i))){
//                break;
//            }
//            output += s.charAt(i);
//        }
//
//    }
    public static void main(String[] args) {
        System.out.println(atoi("+4193 with words"));
    }
}
