package stack;

public class StackUsingQueue {
    int front, rear;
    int[] arr;

    public StackUsingQueue() {
        front = 0;
        rear = 0;
        arr = new int[1000];
    }

    public void push(int x) {
        if(rear < arr.length){
            arr[rear] = x;
            rear++;
        }
    }

    public int pop() {
        if(rear == front || rear < 0){
            return -1;
        }
        int result = arr[rear - 1];
        rear--;
        return result;
    }

    public int top() {
        if(rear == front){
            return -1;
        }
        return arr[rear - 1];
    }

    public boolean empty() {
        return front == rear ? true : false;
    }

    public static void main(String[] args) {
        StackUsingQueue stack = new StackUsingQueue();
        stack.push(1);
        stack.push(2);
        System.out.println(stack.top());
        System.out.println(stack.pop());
        System.out.println(stack.empty());
    }
}
