package greedy;

import java.util.Arrays;
import java.util.Comparator;

class Job {
    int id, profit, deadline;
    Job(int x, int y, int z){
        this.id = x;
        this.deadline = y;
        this.profit = z;
    }
}

class JobComparator implements Comparator<Job> {

    @Override
    public int compare(Job o1, Job o2) {
        // order in descending order
        if(o1.profit > o2.profit)
            return -1;
        else if (o1.profit < o2.profit) {
            return 1;
        } else
            return 0;
    }
}

public class JobScheduling {
    //Function to find the maximum profit and the number of jobs done.
    static Job[] getJobArray(int[] arr, int n){
        Job[] jobs = new Job[n];
        int j = 0;
        for (int i = 0; i < arr.length; i = i + 3) {
            jobs[j] = new Job(arr[i], arr[i+1], arr[i+2]);
            j++;
        }
        return jobs;
    }

    static int[] JobScheduling(Job arr[], int n) {
        int numOfJobs = 0;
        int maxProfit = 0;
        Arrays.sort(arr, new JobComparator());
        int maxTimeLine = Integer.MIN_VALUE;
        for(Job j : arr){
            maxTimeLine = Math.max(maxTimeLine,j.deadline);
        }
        int[] timeline = new int[maxTimeLine];
        for(Job j : arr){
            int deadline = j.deadline;
            while(timeline[deadline - 1] != 0 && deadline >= 0){
                deadline--;
            }
            if(deadline >= 0){
                timeline[deadline] = 1;
                numOfJobs++;
                maxProfit += j.profit;
            }
        }
        int[] answer = {numOfJobs, maxProfit};
        return answer;
    }

    public static void main(String[] args) {
        int[] arr = {1,4,20,2,1,10,3,1,40,4,1,30};
        System.out.println(Arrays.toString(
                JobScheduling(getJobArray(arr,4), 4)
        ));
    }
}
