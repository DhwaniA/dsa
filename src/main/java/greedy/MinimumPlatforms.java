package greedy;

import java.util.Arrays;

public class MinimumPlatforms {
    static int findPlatform(int arr[], int dep[], int n) {
        // add your code here
        int maxPlatformsAtAnyPoint = 1;
        int currentNumberOfPlatforms = 1;
        Arrays.sort(arr);
        Arrays.sort(dep);
        int i = 1, j = 0;
        while(i < n && j < n) {
            if(arr[i] <= dep[j]){
                currentNumberOfPlatforms++;
                i++;
            } else if (arr[i] > dep[j]) {
                currentNumberOfPlatforms--;
                j++;
            }
            maxPlatformsAtAnyPoint = Math.max(currentNumberOfPlatforms,maxPlatformsAtAnyPoint);
        }
        return maxPlatformsAtAnyPoint;
    }
}
