package greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class SJFJob {
    int arrivalTime;
    int burstTime;

    public SJFJob(final int arrivalTime, final int burstTime) {
        this.arrivalTime = arrivalTime;
        this.burstTime = burstTime;
    }
}

class SJFJobComparator implements Comparator<SJFJob> {

    @Override
    public int compare(SJFJob o1, SJFJob o2) {
        if(o1.arrivalTime < o2.arrivalTime)
            return -1;
        else if(o1.arrivalTime > o2.arrivalTime)
            return 1;
//        else if(o1.burstTime < o2.burstTime)
//            return -1;
//        else if (o1.burstTime > o2.burstTime)
//            return 1;
        return 0;
    }
}

public class ShortestJobFirst {
    public static float sjf(int n, int []arrivalTime, int []burstTime)
    {
        List<SJFJob> jobs = new ArrayList<>();
        for (int i = 0; i < arrivalTime.length; i++) {
            jobs.add(new SJFJob(arrivalTime[i], burstTime[i]));
        }
        Collections.sort(jobs, new SJFJobComparator());

        // find average waiting time
        float totalWaitingTime = 0f;
        int currentTime = 0;
        for(int i = 0; i < jobs.size(); i++){
            SJFJob curr = jobs.get(i);
            if(curr.arrivalTime > currentTime){
                currentTime = curr.arrivalTime;
            }
            totalWaitingTime += currentTime - curr.arrivalTime;
            currentTime += curr.burstTime;
        }
        return totalWaitingTime/ jobs.size();
    }
    public static void main(String[] args) {
        int[] arrivalTime = {12, 29, 25, 22, 4, 24, 29, 10, 11};
        int[] burstTime =   {26, 11, 14, 3, 21, 6, 28, 29, 7};
        System.out.println(sjf(9,arrivalTime,burstTime));
    }
}
