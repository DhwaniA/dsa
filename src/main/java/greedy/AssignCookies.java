package greedy;

import java.util.Arrays;
import java.util.Collections;

public class AssignCookies {
    public static int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int i = g.length - 1, j = s.length -1;
        int ans = 0;
        while(i >=0 && j >=0){
            if(s[j] >= g[i]){
                ans++;
                j--;
                i--;
            } else {
                //child cannot be satisfied move to another child
                i--;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] g = {10,9,8,7};
        int[] s = {5,6,7,8};
        System.out.println(findContentChildren(g,s));
    }
}
