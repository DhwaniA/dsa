package greedy;

public class JumpGame {
    public static boolean canJump(int[] nums) {
        int curr = 0;
        int i = 0;
        int n = nums.length;
        while(i < n){
            // find max in the next possible jumps
            int jumps = nums[i];
            int endIndex = Math.min(n-1, i + jumps);
            int localMaxIndex = -1;
            for(int j = i+1; j<= endIndex; j++){
                if (localMaxIndex == -1){
                    localMaxIndex = j;
                } else {
                    if(nums[j] >= nums[localMaxIndex]){
                        localMaxIndex = j;
                    }
                }
            }
            if(localMaxIndex != -1){
                curr = localMaxIndex;
                i = localMaxIndex;
            } else {
                break;
            }
        }

        return curr >= n -1;
    }

    public static void main(String[] args) {
        int[] nums = {2,3,1,1,4};
        System.out.println(canJump(nums));
    }
}
