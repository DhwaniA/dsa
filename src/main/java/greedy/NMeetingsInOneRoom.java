package greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Meeting {
    int start;
    int end;
    int pos;

    public Meeting(final int start, final int end, final int pos) {
        this.start = start;
        this.end = end;
        this.pos = pos;
    }
}

class MeetingComparator implements Comparator<Meeting> {

    @Override
    public int compare(Meeting o1, Meeting o2) {
        if (o1.end < o2.end)
            return -1;
        else if (o1.end > o2.end) {
            return 1;
        } else if (o1.pos < o2.pos) {
            return -1;
        } else
            return 1;
    }
}
public class NMeetingsInOneRoom {
    public static int maxMeetings(int start[], int end[], int n) {
        // add your code here
        List<Meeting> meetings = new ArrayList<>();
        for (int i = 0; i < start.length; i++) {
            meetings.add(new Meeting(start[i], end[i], i+1));
        }
        Collections.sort(meetings, new MeetingComparator());
        List<Integer> result = new ArrayList<>();
        result.add(meetings.get(0).pos);
        int limit = meetings.get(0).end;
        for (int i = 1; i < meetings.size(); i++) {
            Meeting curr = meetings.get(i);
            if(curr.start > limit){
                result.add(curr.pos);
                limit = curr.end;
            }
        }
        return result.size();
    }
}
