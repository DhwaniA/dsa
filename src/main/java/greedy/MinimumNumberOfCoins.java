package greedy;

import java.util.ArrayList;
import java.util.List;

public class MinimumNumberOfCoins {
    public static List<Integer> MinimumCoins(int n) {
        // Write your code here.
        List<Integer> result = new ArrayList<>();
        int[] coins = {1, 2, 5, 10, 20, 50, 100, 500, 1000};
        for (int i = coins.length - 1 ; i >= 0 ; i--) {
            while (coins[i] <= n){
                result.add(coins[i]);
                n -= coins[i];
            }
        }
        return result;
    }
}
