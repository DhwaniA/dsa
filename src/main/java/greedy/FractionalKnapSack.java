package greedy;

import java.util.Arrays;
import java.util.Comparator;

class Item {
    int value, weight;
    Item(int x, int y){
        this.value = x;
        this.weight = y;
    }
}

class CustomComparator implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        // we will compare by value per unit weight
        double first = (double) o1.value / (double) o1.weight;
        double second = (double) o2.value / (double) o2.weight;
        // we wish to sort in descending order
        if(first > second)
            return -1;
        else if(first < second)
            return 1;
        else
            return 0;
    }
}
public class FractionalKnapSack {
    static double fractionalKnapsack(int W, Item arr[], int n) {
        // Your code here
        double maxValue = 0.0;
        Arrays.sort(arr, new CustomComparator());
        for (Item item :
                arr) {
            if (item.weight <= W) {
               maxValue += item.value;
               W -= item.weight;
            } else if (item.weight > W) {
                maxValue += W * ((double) item.value / (double) item.weight);
                break;
            }
        }
        return maxValue;
    }
}
