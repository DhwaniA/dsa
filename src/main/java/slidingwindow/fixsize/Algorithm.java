package slidingwindow.fixsize;

public class Algorithm {
    static void solve ( int[] arr, int k){
        // i = start of window
        // j = end of window
        int i = 0, j = 0;
        // window size = j-i+1
        while(j < arr.length){
            //do some calculations
            if( j - i + 1 < k){
                // until we hit the window size our only job is to increment j
                j++;
            } else if ( j - i + 1 == k){
                // find answer from the calculations made
                // remove the calculations for i
                // slide the window
                i++;
                j++;
            }
        }
    }
}
