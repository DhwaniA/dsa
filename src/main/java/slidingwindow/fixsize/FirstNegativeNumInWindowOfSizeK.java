package slidingwindow.fixsize;

import java.util.ArrayList;
import java.util.List;

public class FirstNegativeNumInWindowOfSizeK {
    static void firstNegative(int[] arr, int k){
        int i = 0, j = 0 ;
        int firstNegative = 0;
        List<Integer> negNums = new ArrayList<>();
        // window size = j - i + 1
        while ( j < arr.length) {
            if (arr[j] < 0) negNums.add(arr[j]);
            if ( j - i + 1 < k){
                j++;
            } else if ( j - i + 1 == k){
                System.out.print( negNums.isEmpty() ? 0 : negNums.get(0) + " ");
                if(arr[i] < 0) {
                    negNums.remove(0);
                }
                j++;
                i++;
            }
        }
    }

    public static void main(String[] args) {
        int arr[] = {12, -1, -7, 8, -15, 30, 16, 28};
        int k = 3;
        firstNegative(arr, k);
    }
}
