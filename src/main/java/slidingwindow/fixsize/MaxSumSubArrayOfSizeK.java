package slidingwindow.fixsize;

public class MaxSumSubArrayOfSizeK {
    static int maxSumSubArray( int[] arr, int k){
        int i = 0, j = 0;
        int maxSum = Integer.MIN_VALUE;
        // first construct the window of size K
        //int windowSize = j - i + 1;
        int sum = 0;
        while ( j < arr.length) {
            sum += arr[j];
            // if window size id not reached keep incrementing the end pointer
            if (j - i + 1 < k){
                j++;
            }
            // if window size reached make the necessary calculations
            else if ( j - i + 1 == k){
                maxSum = Math.max(maxSum,sum);
                sum -= arr[i];
                i++;
                j++;
            }
        }
        return maxSum;
    }

    public static void main(String[] args) {
        int arr[] = { 1, 4, 2, 10, 2, 3, 1, 0, 20 };
        int k = 4;
        System.out.println(maxSumSubArray(arr,k));
    }
}
