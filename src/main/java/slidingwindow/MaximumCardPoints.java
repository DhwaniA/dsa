package slidingwindow;

public class MaximumCardPoints {
    static int minSumSubArray( int[] arr, int k){
        int i = 0, j = 0;
        int minSum = Integer.MAX_VALUE;
        // first construct the window of size K
        //int windowSize = j - i + 1;
        int sum = 0;
        while ( j < arr.length) {
            sum += arr[j];
            // if window size id not reached keep incrementing the end pointer
            if (j - i + 1 < k){
                j++;
            }
            // if window size reached make the necessary calculations
            else if ( j - i + 1 == k){
                minSum = Math.min(minSum,sum);
                sum -= arr[i];
                i++;
                j++;
            }
        }
        return minSum;
    }
    // k = no. of cards you can pick from either sides
    static int solve(int[] cardPoints, int k){
        // basically find a window of size n-k with minimum sum, the remaining will give maximum sum
        int totalSum = 0;
        for (int i = 0; i < cardPoints.length; i++) {
            totalSum += cardPoints[i];
        }

        int minSum = minSumSubArray(cardPoints, cardPoints.length - k);
        return totalSum - minSum;
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,1};
        int k = 3;
        System.out.println(solve(arr, k));
    }
}
