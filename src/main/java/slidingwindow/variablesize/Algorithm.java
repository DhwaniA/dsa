package slidingwindow.variablesize;

public class Algorithm {
    static void solve ( int[] arr, int k){
        // i = start of window
        // j = end of window
        int ans = 0;
        int i = 0, j = 0;
        int condition = 0;
        // window size = j-i+1
        while(j < arr.length){
            //do some calculations
            if( condition < k){
                // until we hit the window size our only job is to increment j
                j++;
            } else if ( condition == k){
                // find answer from the calculations made
                j++;
            } else if ( condition > k ){
                while ( condition > k){
                    // remove calculations for i
                    i++;
                }
                j++;
            }
        }
        // return ans
    }
}
