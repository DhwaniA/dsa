package slidingwindow.variablesize;

public class LongestSubArrayOfSumK {
    //will work only for positive values
    static int solve(int[] arr, int sumK){
        int maxSize = Integer.MIN_VALUE;
        int i =0, j = 0;
        long sum = 0;
        while(j < arr.length) {
            sum += arr[j];
            if(sum < sumK){
                j++;
            } else if ( sum == sumK) {
                // update the max subarray size
                maxSize = Math.max(maxSize, j - i + 1);
                // now move to the next candidate
                j++;
            }  else if ( sum > sumK ){
                // we need to have sum less than k only then we will have some other solution
                while ( sum > sumK ){
                    sum -= arr[i];
                    i++;
                }
                j++;
            }
        }
        return maxSize;
    }

    public static void main(String[] args) {
        int arr[] = {4,1,1,1,2,3,5};
        int k = 5;
        System.out.println(solve(arr,k));
    }
}
