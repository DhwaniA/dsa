package slidingwindow.variablesize;

import java.util.HashMap;
import java.util.Map;

public class LongestSubStringWithoutRepeatingChars {
    // similar to longest substring with k unique chars, here the value of k will be length of the window
    static int lengthOfLongestSubstring(String s) {
        int result = Integer.MIN_VALUE;
        // k = number of unique characters
        int i = 0, j = 0;
        Map<Character, Integer> countMap = new HashMap<>();
        while(j < s.length()){
            // calculations for j
            countMap.compute(s.charAt(j), (key, value) -> value == null ? 1 : value + 1);
            if( countMap.size() > j-i+1){
                // this condition will never hit
                // we don't have required number of unique chars so keep incrementing j
                //j++;
            } else if (countMap.size() == j-i+1){
                // it is a potential solution
                result = Math.max(result, j - i + 1);
                j++;
            } else if (countMap.size() < j-i+1) {
                // if distinct chars are less then window size means we have repeating letters
                while ( countMap.size() < j-i+1){
                    // remove calculations for i
                    countMap.computeIfPresent(s.charAt(i), (key, val) -> val - 1 == 0 ? null : val - 1);
                    i++;
                }
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        String s = "pwwkew";
        System.out.println(lengthOfLongestSubstring(s));
    }
}
