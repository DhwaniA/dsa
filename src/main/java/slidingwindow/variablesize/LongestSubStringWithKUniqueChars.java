package slidingwindow.variablesize;

import java.util.HashMap;
import java.util.Map;

public class LongestSubStringWithKUniqueChars {
    static int solve ( String s, int k){
        int result = Integer.MIN_VALUE;
        // k = number of unique characters
        int i = 0, j = 0;
        Map<Character, Integer> countMap = new HashMap<>();
        while(j < s.length()){
            // calculations for j
            countMap.compute(s.charAt(j), (key, value) -> value == null ? 1 : value + 1);
            if( countMap.size() < k){
                // we don't have required number of unique chars so keep incrementing j
                j++;
            } else if (countMap.size() == k){
                // it is a potential solution
                result = Math.max(result, j - i + 1);
                j++;
            } else if (countMap.size() > k) {
                while ( countMap.size() > k){
                    // remove calculations for i
                    countMap.computeIfPresent(s.charAt(i), (key, val) -> val - 1 == 0 ? null : val - 1);
                    i++;
                }
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        String s = "aabacbebebe";
        int k = 3;
        System.out.println(solve(s,k));
    }
}
