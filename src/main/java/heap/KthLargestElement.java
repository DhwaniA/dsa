package heap;

import java.util.PriorityQueue;

public class KthLargestElement {
    static int solve(int[] nums, int k){
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int num :
                nums) {
            minHeap.add(num);
            if(minHeap.size() > k){
                minHeap.poll();
            }
        }
        return minHeap.peek();
    }

    public static void main(String[] args) {
        int[] nums = new int[]{7,10,4,3,20,15};
        System.out.println(solve(nums,3));
    }
}
