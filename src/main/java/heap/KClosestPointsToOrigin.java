package heap;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class DistancePair {
    double distance;
    int x;
    int y;

    public DistancePair(int x, int y) {
        this.x = x;
        this.y = y;
        distance = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
    }
}
public class KClosestPointsToOrigin {
    public int[][] kClosest(int[][] points, int k) {
        // Sort in ascending order of frequency
        Comparator<DistancePair> comparator = Comparator.comparing(((DistancePair p) -> p.distance)).reversed();
        PriorityQueue<DistancePair> maxHeap = new PriorityQueue<>(comparator);

        for (int[] point :
                points) {
            maxHeap.add(new DistancePair(point[0], point[1]));
            if(maxHeap.size() > k){
                maxHeap.poll();
            }
        }
        int[][] result = new int[k][2];
        int i = 0 ;
        while (!maxHeap.isEmpty()){
            DistancePair pair = maxHeap.poll();
            result[i][0] = pair.x;
            result[i][1] = pair.y;
            i++;
        }
        return result;
    }
}
