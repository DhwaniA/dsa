package heap;

import java.util.*;

class RankPair{
    int val;
    int position;

    public RankPair(final int val, final int position) {
        this.val = val;
        this.position = position;
    }
}
public class ReplaceElementWithRank {
    public static List<Integer> replaceWithRank(List<Integer> arr, int n) {
        PriorityQueue<RankPair> minHeap = new PriorityQueue<>(Comparator.comparingInt(p -> p.val));
        for (int i = 0; i < arr.size(); i++) {
            minHeap.add(new RankPair(arr.get(i),i));
        }
        int rank = 1;
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i < arr.size(); i++){
            result.add(0);
        }
        while (!minHeap.isEmpty()){
            RankPair current = minHeap.poll();
            result.set(current.position, rank);
            rank++;
        }

        return result;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2,6,9,2));
        replaceWithRank(list,2);
    }
}
