package heap;

import java.util.PriorityQueue;

public class KthSmallestElement {
    static int solve(int[] nums, int k){
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>((a, b) -> b - a);
        for (int num :
                nums) {
            maxHeap.add(num);
            if(maxHeap.size() > k){
                maxHeap.poll();
            }
        }
        return maxHeap.peek();
    }

    public static void main(String[] args) {
        int[] nums = new int[]{7,10,4,3,20,15};
        System.out.println(solve(nums,3));
    }
}
