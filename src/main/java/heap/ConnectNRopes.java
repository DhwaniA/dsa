package heap;

import java.util.PriorityQueue;

public class ConnectNRopes {
    /*
    At any time connecting 2 smallest ropes will result in minimum cost, thus using min Heap
     */
    static int minCost(int arr[], int n) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int num :
                arr) {
            minHeap.add(num);
        }
        int cost = 0;
        while (!minHeap.isEmpty()){
            Integer firstRope = minHeap.poll();
            Integer secondRope = minHeap.poll();
            if(secondRope == null){ // means we have connected all the ropes
                return cost;
            }
            minHeap.add(firstRope + secondRope);
            cost += firstRope + secondRope;
        }
        return cost;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5};
        System.out.println(minCost(arr,5));
    }
}
