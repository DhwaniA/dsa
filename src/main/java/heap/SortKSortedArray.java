package heap;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

// K sorted array == nearly sorted array
public class SortKSortedArray {
    static List<Integer> solve(int[] nums, int k){
        // create a min heap because everytime we need the smallest element
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        List<Integer> result = new ArrayList<>();
        for (int num :
                nums) {
            minHeap.add(num);
            if(minHeap.size() > k){
                result.add(minHeap.poll());
            }
        }
        while ( !minHeap.isEmpty() ){
            result.add(minHeap.poll());
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 1, 5, 6, 4};
        System.out.println(solve(nums, 2));
    }
}
