package heap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class HandOfStraights {
    public static boolean findGroups(int []cards, int groupsize) {
        if(cards.length % groupsize != 0){
            return false;
        }
        Arrays.sort(cards);
        Map<Integer, PriorityQueue<Integer>> maxHeaps = new HashMap<>();
        int numberOfHeaps = cards.length/groupsize;
        for (int i = 1; i <= numberOfHeaps; i++) {
            maxHeaps.put(i,new PriorityQueue<Integer>((a, b) -> b - a));
        }

        for (int card :
                cards) {
            for (Map.Entry<Integer, PriorityQueue<Integer>> entry : maxHeaps.entrySet()) {
                PriorityQueue<Integer> heap = entry.getValue();
                if (heap.isEmpty() || (Math.abs(heap.peek() - card) == 1 && heap.size() < groupsize)) {
                    heap.add(card);
                    break;
                }
            }
        }

        for (Map.Entry<Integer, PriorityQueue<Integer>> entry : maxHeaps.entrySet()) {
            PriorityQueue<Integer> heap = entry.getValue();
            if (heap.size() != groupsize) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        int[] cards = new int[]{2,3,1};
        System.out.println(findGroups(cards,3));
    }
}
