package heap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

class Pair implements Comparable<Pair>{
    int absDifference;
    int number;

    public Pair(int absDifference, int number) {
        this.absDifference = absDifference;
        this.number = number;
    }

    public int compareTo(Pair o)
    {
        // If there are two elements with the same
        // difference with X, the greater element is
        // given priority.
        if (absDifference == o.absDifference)
            return o.number - number;
        else
            return o.absDifference - absDifference;
    }
}
public class KClosestNumbers {

    static List<Integer> solve(int[] nums, int k, int x){
        PriorityQueue<Pair> maxHeap = new PriorityQueue<>();
        for (int num :
                nums) {
            maxHeap.add(new Pair(Math.abs(num - x), num));
            if (maxHeap.size() > k){
                maxHeap.poll();
            }
        }

        List<Integer> result = new ArrayList<>();
        for (Pair p :
                maxHeap) {
            result.add(0, p.number);
        }
        return result;
    }

    public static void main(String[] args) {
        // TODO Not accepted in Leetcode
        int[] nums = new int[]{10, 2, 14, 6, 7, 4, 11};
        System.out.println(solve(nums, 3, 5));
    }
}
