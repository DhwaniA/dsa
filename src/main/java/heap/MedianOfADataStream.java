package heap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

class MedianFinder {
    int medianPosition;
    PriorityQueue<Integer> maxHeap;
    public MedianFinder() {
        maxHeap = new PriorityQueue<>((a, b) -> b - a);
    }

    public void addNum(int num) {
        maxHeap.add(num);
        medianPosition = (int) Math.ceil(((double)(maxHeap.size() + 1))/2);
    }

    public double findMedian() {
        List<Integer> temp = new ArrayList<>();
        int currentSize = maxHeap.size();

        while(maxHeap.size() > medianPosition){
            temp.add(maxHeap.poll());
        }
        if(currentSize % 2 != 0){
            return maxHeap.peek();
        }
        int tempInt = maxHeap.poll();
        double median =((double) (tempInt + maxHeap.peek()))/2;
        maxHeap.add(tempInt);

        for (int num :
                temp) {
            maxHeap.add(num);
        }
        return median;
    }
}

public class MedianOfADataStream {
    public static void main(String[] args) {
        MedianFinder finder = new MedianFinder();
        finder.addNum(1);
        finder.addNum(2);
        System.out.println(finder.findMedian());
        finder.addNum(3);
        System.out.println(finder.findMedian());
    }
}
