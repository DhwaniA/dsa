package heap;

import java.util.*;

class FPair {
    int number;
    int frequency;

    public FPair(int number, int frequency) {
        this.number = number;
        this.frequency = frequency;
    }
}
public class TopKFrequentNumbers {
    public static int[] topKFrequent(int[] nums, int k) {
        // Find most freq ( large ) we will make a min heap
        PriorityQueue<FPair> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a.frequency));
        
        // Find frequency of all elements
        Map<Integer,Integer> frequencyMap = new HashMap<>();
        for (int num :
                nums) {
            frequencyMap.compute(num, (key, val) -> (val == null) ? 1 : val + 1);
        }

        // append elements to the heap
        for (Map.Entry<Integer,Integer> entry : frequencyMap.entrySet()){
            minHeap.add(new FPair(entry.getKey(), entry.getValue()));
            if(minHeap.size() > k){
                minHeap.poll();
            }
        }

//        Integer[] result = minHeap.stream().map(fPair -> fPair.number).toArray(Integer[]::new);
        int[] result = new int[minHeap.size()];
        int i = 0;
        while(!minHeap.isEmpty()){
            result[i] = minHeap.poll().number;
            i++;
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums= new int[]{1,1,1,2,2,3};
        Arrays.stream(topKFrequent(nums, 2)).forEach(System.out::print);
    }
}
