package heap;

import java.util.*;

class ListNode {
  int val;
  ListNode next;
  ListNode() {}
  ListNode(int val) { this.val = val; }
  ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}


// TODO
public class MergeKSortedArrays {
    public static ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> minHeap = new PriorityQueue<>(Comparator.comparingInt(n -> n.val));
        // Insert the first element of each k lists in the minHeap
        for (int i = 0; i < lists.length; i++) {
            if(lists[i] != null)
                minHeap.add(lists[i]);
        }
        ArrayList<Integer> result = new ArrayList<>();
        while(!minHeap.isEmpty()){
            ListNode current = minHeap.poll();
            result.add(current.val);
            if(current.next != null)
                minHeap.add(current.next);
        }
        return getLinkedList(result);
    }

    static ListNode getLinkedList(List<Integer> list){
        ListNode head = null;
        if(list.size() >= 1){
            head = new ListNode(list.get(0),null);
        }

        ListNode prev = head;

        for (int i = 1; i < list.size(); i++) {
            ListNode current = new ListNode(list.get(i),null);
            prev.next = current;
            prev = current;
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode firstList = getLinkedList(Arrays.asList(1));
        ListNode[] input = new ListNode[]{firstList};
        mergeKLists(input);
    }
}
