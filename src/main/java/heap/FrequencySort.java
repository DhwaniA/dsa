package heap;

import java.util.*;

/*
Given an array of integers nums, sort the array in increasing order based on the frequency of the values.
If multiple values have the same frequency, sort them in decreasing order.
 */
public class FrequencySort {
    static int[] frequencySort(int[] nums) {
        // Sort in ascending order of frequency
        Comparator<FPair> comparator = Comparator.comparing(((FPair p) -> p.frequency)).thenComparing(p -> p.number, Comparator.reverseOrder());
        PriorityQueue<FPair> minHeap = new PriorityQueue<>(comparator);

        // Find frequency of all elements
        Map<Integer,Integer> frequencyMap = new HashMap<>();
        for (int num :
                nums) {
            frequencyMap.compute(num, (key, val) -> (val == null) ? 1 : val + 1);
        }

        // append elements to the heap
        for (Map.Entry<Integer,Integer> entry : frequencyMap.entrySet()){
            minHeap.add(new FPair(entry.getKey(), entry.getValue()));
        }

//        Integer[] result = minHeap.stream().map(fPair -> fPair.number).toArray(Integer[]::new);
        int i = 0;
        while(!minHeap.isEmpty()){
            int num = minHeap.peek().number;
            int freq = minHeap.peek().frequency;
            for (int j = 0; j < freq; j++) {
                nums[i] = num;
                i++;
            }
            minHeap.poll();
        }
        return nums;
    }

    public static void main(String[] args) {
        int[] nums= new int[]{2,3,1,3,2};
        Arrays.stream(frequencySort(nums)).forEach(System.out::print);
    }
}
