package heap;

import java.util.*;
import java.util.stream.Collectors;

class Tweet {
    int userId;
    int tweetId;
    int time;

    public Tweet(final int userId, final int tweetId, final int time) {
        this.userId = userId;
        this.tweetId = tweetId;
        this.time = time;
    }
}
class Twitter {
    static int timestamp = 0;
    Map<Integer, LinkedList<Tweet>> tweets;
    Map<Integer, List<Integer>> followers;
    public Twitter() {
        tweets = new HashMap<>();
        followers = new HashMap<>();
    }

    public void postTweet(int userId, int tweetId) {
        tweets.computeIfAbsent(userId, key -> new LinkedList<>()).add(new Tweet(userId,tweetId, timestamp++));
    }

    public List<Integer> getNewsFeed(int userId) {
        //since we want the latest articles we will create a min heap
        PriorityQueue<Tweet> maxHeap = new PriorityQueue<>(Comparator.comparing(a -> a.time, Comparator.reverseOrder()));
        //first fetch followers
        List<Integer> followers = this.followers.getOrDefault(userId, new ArrayList<Integer>());
        for (int f :
                followers) {
            LinkedList<Tweet> tweetList = tweets.get(f);
            addTweetsToHeap(tweetList,maxHeap,10);
        }
        // add the current users tweets also to the heap
        LinkedList<Tweet> tweetList = tweets.get(userId);
        addTweetsToHeap(tweetList,maxHeap,10);

        List<Integer> result = maxHeap.stream().map(tweet -> tweet.tweetId).collect(Collectors.toList());
        Collections.reverse(result);
        return result;
    }

    private void addTweetsToHeap(List<Tweet> tweets, PriorityQueue<Tweet> heap, int heapSize){
        for (Tweet t :
                tweets) {
            heap.add(t);
            if(heap.size() > heapSize){
                heap.poll();
            }
        }
    }

    public void follow(int followerId, int followeeId) {
        followers.computeIfAbsent(followerId, key -> new ArrayList<>()).add(followeeId);
    }

    public void unfollow(int followerId, int followeeId) {
        followers.computeIfPresent(followerId, (k, v) -> {v.remove((Integer) followeeId); return v;});
    }
}

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter obj = new Twitter();
 * obj.postTweet(userId,tweetId);
 * List<Integer> param_2 = obj.getNewsFeed(userId);
 * obj.follow(followerId,followeeId);
 * obj.unfollow(followerId,followeeId);
 */
