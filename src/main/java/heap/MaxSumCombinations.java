package heap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

public class MaxSumCombinations {
    public ArrayList<Integer> solve(ArrayList<Integer> A, ArrayList<Integer> B, int C) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < A.size(); i++) {
            for (int j = 0; j < B.size(); j++) {
                minHeap.add(A.get(i) + B.get(j));
                if(minHeap.size() > C){
                    minHeap.poll();
                }
            }
        }
        ArrayList<Integer> result = new ArrayList<>();
        while (!minHeap.isEmpty()){
            result.add(0,minHeap.poll());
        }
        return result;
    }
}
