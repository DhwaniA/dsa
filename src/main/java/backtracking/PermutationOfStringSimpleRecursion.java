package backtracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PermutationOfStringSimpleRecursion {
    // Let's try solving this first with recursion
    static List<String> getPermutations(String s){
        List<String> result = new ArrayList<>();
        permute(s, "", result);
        return result;
    }

    static void permute(String ip, String op, List<String> result){
        if(ip.isEmpty()){
            result.add(op);
            return;
        }
        // remember the choices already made -- as string can have duplicate values
        Set<Character> choicesMade = new HashSet<>();

        // number of choices is variable - so we have a for loop
        for(int i = 0; i < ip.length(); i++){
            char c = ip.charAt(i);
            // eliminate branches
            if(!choicesMade.contains(c)) {
                choicesMade.add(c);
                String newOutput = op + c;
                String newInput = ip.substring(0, i) + ip.substring(i + 1);
                permute(newInput, newOutput, result);
            }
        }
    }
    // code slowly morphed itself to backtracking algorithm
    // this is still not backtracking as we are passing the input by value and not by reference
    public static void main(String[] args) {
        getPermutations("abc").forEach(System.out::println);
    }
}
