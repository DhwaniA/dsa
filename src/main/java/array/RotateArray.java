package array;

public class RotateArray {
    /**
     * Textbook solution - Find the pivot element ( minimum element ) in the array
     * Both the arrays before and after the pivot element will be sorted in increasing order
     * Check if last element is smaller than the first element to ensure the array has been rotated at least once
     * @param nums
     * @return
     */
    public static boolean isSortedAndRotated(int[] nums){
        int n = nums.length;
        boolean flagDip = false;
        for(int i = 0, j = 1; i < n-1; i++,j++) {
            if (nums[j] < nums[i]){
                if (!flagDip) {
                    flagDip = true;
                    continue;
                }
                return false;
            }
        }
        if(flagDip){
            return nums[0] > nums[n-1];
        }
        return true;
    }

    public static void main(String[] args) {
        int[] nums = {4,5,1,2,3};
        System.out.println(isSortedAndRotated(nums));
    }
}
