package array;

public class BuyAndSellStock {
    public static int maxProfit(int[] prices) {
        int n = prices.length;
        int[] maxArray = new int[n];
        maxArray[n-1] = prices[n-1];
        for (int i = n -2; i >= 0; i--) {
            maxArray[i] = Math.max(prices[i], maxArray[i+1]);
        }
        int maxProfit = 0;
        for (int i = 0; i < n; i++) {
            maxProfit = Math.max(maxArray[i] - prices[i], maxProfit);
        }
        return maxProfit;
    }
}
/*
 * Generate an Array which stores the maximum of all the days from i+1 to n
 * Maximum profit is the MAX of the diff between maxArray[i] - prices[i]
 */