package array;

import java.util.Arrays;

public class LeftRotateArray {
    public static int[] leftRotate(int[] nums, int k){
        int[] res = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int index = (i-k+ nums.length) % nums.length;
            res[index] = nums[i];
        }
        return res;
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5};
        Arrays.stream(leftRotate(nums, 2)).forEach(System.out::print);
    }
}
