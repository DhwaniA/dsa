package array;

/**
 * Every number in array appears twice. Find the number which appears only once
 */
public class SingleNumber {

    /**
     * Bit Manipulation
     * XOR operation is associative ( order does not matter )
     * XOR with 0 returns the number itself
     */
    public int singleNumber(int[] nums) {
        int singleNumber = 0;
        for(int n : nums){
            singleNumber ^= n;
        }
        return singleNumber;
    }
}
