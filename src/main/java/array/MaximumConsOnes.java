package array;

public class MaximumConsOnes {
    public int findMaxConsecutiveOnes(int[] nums) {
        int maxSum = 0;
        int sum = 0;
        for(int n : nums){
            if(sum == sum + n){
                maxSum = Math.max(sum, maxSum);
                sum = 0;
            } else {
                sum += n;
            }
        }
        return Math.max(sum, maxSum);
    }
}
