package array;

import java.util.ArrayList;
import java.util.List;

public class Merge2SortedArrays {

    /**
     * Find union of 2 sorted arrays - resulting array should not have duplicates
     * Combination problem - remove duplicates from individual arrays + merge 2 arrays( 2 pointer )
     */
    public static List<Integer> union(int[] a, int[] b){
        // Write your code here
        List<Integer> result = new ArrayList<>();
        int i = 0, j = 0;
        while ( i < a.length && j < b.length ){
            if(a[i] < b[j]){
                result.add(a[i]);
                i++;
            } else if ( a[i] == b [j]){
                result.add(a[i]);
                i++;
                j++;
            } else {
                result.add(b[j]);
                j++;
            }
        }

        while(i < a.length){
            result.add(a[i]);
            i++;
        }

        while(j < b.length){
            result.add(b[j]);
        }
        return result;
    }
}
