package array;

import java.util.Arrays;

public class MoveZeroesToEnd {
    /**
     * Two pointer solution
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int pointer = 0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != 0){
                nums[pointer] = nums[i];
                pointer++;
            }
        }
        while(pointer < nums.length){
            nums[pointer] = 0;
            pointer++;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,2,0,3,0,1};
        moveZeroes(nums);
        Arrays.stream(nums).forEach(System.out::print);
    }
}
