package array;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveDuplicatesFromSortedArray {
    public static int removeDuplicates(ArrayList<Integer> arr, int n) {
        for (int i = 1; i < arr.size(); i++) {
            if(arr.get(i-1) == arr.get(i)){
                arr.remove(i-1);
                i--;
            }
        }
        return arr.size();
    }

    // Change the elements in place and return count of the new array
    public static int removeDuplicates(int[] nums) {
        int numOfShifts = 0;
        for(int i = 1; i < nums.length; i++){
            if(nums[i] == nums[i-1]){
                // TODO
            }
        }
        return numOfShifts;
    }
    public static void main(String[] args) {
        System.out.println(removeDuplicates(new ArrayList<Integer>(Arrays.asList(1,2,2,2,3,3,4,4,5)), 9));
    }
}
