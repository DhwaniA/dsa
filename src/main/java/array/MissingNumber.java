package array;

public class MissingNumber {
    public static int missingNumber(int[] nums) {
        int n = nums.length;
        int sum = ((n+1)*n)/2;
        for(int i : nums){
            sum -= i;
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] nums = {3,0,1};
        System.out.println(missingNumber(nums));
    }
}
