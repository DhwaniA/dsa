package array;

import java.util.ArrayList;
import java.util.List;

public class LeadersInAnArray {
    public static List< Integer > superiorElements(int []a) {
        List<Integer> result = new ArrayList<>();
        int[] maxUntilIndex = new int[a.length];
        maxUntilIndex[a.length -1] = a[a.length -1];
        result.add(a[a.length -1]);
        for (int i = a.length - 2; i >= 0; i--) {
            maxUntilIndex[i] = Math.max(a[i],maxUntilIndex[i+1]);
            if(maxUntilIndex[i] == a[i] && a[i] != maxUntilIndex[i+1]){
                result.add(a[i]);
            }
        }
        return result;
    }
}
