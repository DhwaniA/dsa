package trees.binary;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
     TreeNode() {}
     TreeNode(int val) { this.val = val; }
     TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
 }

public class LevelOrderTraversal {
    public List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> result = new LinkedList<>();
        if (root == null) return result;
        queue.offer(root);
        while (!queue.isEmpty()){
            List<TreeNode> currentLevel = new LinkedList<>();
            while (!queue.isEmpty()){
                TreeNode current = queue.poll();
                currentLevel.add(current);
            }
            for (TreeNode node : currentLevel){
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null){
                    queue.offer(node.right);
                }
            }
            List<Integer> currentLevelValues = new LinkedList<>();
            for (TreeNode node : currentLevel){
                currentLevelValues.add(node.val);
            }
            result.add(currentLevelValues);
        }
        return result;
    }
}
