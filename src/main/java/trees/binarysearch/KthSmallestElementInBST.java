package trees.binarysearch;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class KthSmallestElementInBST {
    public int kthSmallest(TreeNode root, int k) {
        int[] ans = new int[1];
        inorder(root, k, ans);
        return ans[0];
    }

    public void inorder(TreeNode root, int k, int[] ans){
        if(root == null){
            return;
        }
        inorder(root.left, k, ans);
        if(k == 1){
            ans[0] = root.val;
        }
        inorder(root.right, k -1, ans);
    }
}
