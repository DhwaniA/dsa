package maths;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Frequency {
    public static int[] getFrequencies(int []v) {
        // Write Your Code Here
        Map<Integer,Integer> frequency = new HashMap<>();
        for (int i : v) {
            if(frequency.containsKey(i)){
                frequency.put(i, frequency.get(i) + 1);
            } else {
                frequency.put(i, 1);
            }
        }
        int minFreq = v.length;
        int minFreqKey = -1;
        int maxFreq = 0;
        int maxFreqKey = -1;
        for(Map.Entry<Integer, Integer> entry : frequency.entrySet()){
            if(entry.getValue() < minFreq){
                minFreq = entry.getValue();
                minFreqKey = entry.getKey();
            }
            if(entry.getValue() > maxFreq){
                maxFreq = entry.getValue();
                maxFreqKey = entry.getKey();
            }
            if(entry.getValue() == minFreq && entry.getKey() < minFreqKey){
                minFreqKey = entry.getKey();
            }
            if(entry.getValue() == maxFreq && entry.getKey() < maxFreqKey){
                maxFreqKey = entry.getKey();
            }
        }
        int[] result = {maxFreqKey, minFreqKey};
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 1, 1, 4};
        System.out.println(Arrays.toString(Frequency.getFrequencies(nums)));
    }
}
