package maths;

import java.util.Arrays;

public class MergeSort {
    public static void mergeSort(int[] arr, int l, int r){
        // Write your code here
        int n = arr.length;
        if(n == 1){
            return;
        }
        int mid = l + (r-l)/2;
        int[] left = new int[mid];
        int[] right = new int[n-mid];
        for (int i = l; i < mid; i++) {
            left[i] = arr[i];
        }
        for (int i = mid; i <= r; i++) {
            right[i - mid] = arr[i];
        }
        mergeSort(left, 0, left.length - 1);
        mergeSort(right, 0, right.length -1);
        merge(left, right, arr,l,r);
    }

    public static void merge(int[] left, int[] right, int[] arr, int lefti, int righti){
        int l = 0, r = 0, k = lefti;
        int ll = left.length;
        int rl = right.length;
        while(l < ll && r < rl){
            if(left[l] <= right[r]){
                arr[k] = left[l];
                l++;
            } else {
                arr[k] = right[r];
                r++;
            }
            k++;
        }
        while(l < ll){
            arr[k] = arr[l];
            l++;
            k++;
        }
        while(r < rl){
            arr[k] = arr[r];
            r++;
            k++;
        }
    }

    public static void main(String[] args) {
        int[] nums = {2, 13, 4, 1, 3, 6, 28};
        MergeSort.mergeSort(nums,0,6);
        System.out.println(Arrays.toString(nums));
    }
}
