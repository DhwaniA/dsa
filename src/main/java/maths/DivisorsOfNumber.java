package maths;

import java.util.ArrayList;
import java.util.List;

public class DivisorsOfNumber {
    public static List<Integer> getDistinctDivisors(int n){
        List<Integer> divisors = new ArrayList<>();

        // We only need to traverse till the squareRoot of the number
        for (int i = 1; i <= Math.sqrt(n); i++) {
            if(n % i == 0){
                divisors.add(i);
                if(i != (n/i))
                    divisors.add(n/i);
            }
        }
        return divisors;
    }

    public static void main(String[] args) {
        System.out.println(getDistinctDivisors(4));
    }
}
