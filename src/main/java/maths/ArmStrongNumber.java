package maths;

import java.util.Map;

public class ArmStrongNumber {
    public static boolean isArmStrongNumber(int n){
        int numberOfDigits = Integer.toString(n).length();
        int armStrongNumber = 0;
        int originalNumber = n;
        while(n != 0){
            int digit = n % 10;
            n = n/10;
            armStrongNumber += (int) Math.pow(digit, numberOfDigits);
        }
        return armStrongNumber == originalNumber;
    }

    public static void main(String[] args) {
        System.out.println(isArmStrongNumber(371));
    }
}
