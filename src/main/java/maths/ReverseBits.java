package maths;

public class ReverseBits {
    public static int reverseBits(int n){
        int reverse = 0;
        for(int i = 0; i < Integer.SIZE; i++){
            reverse <<= 1;
            if(( n & 1 ) == 1){
                reverse ^= 1;
            }
            n >>= 1;
        }
        return reverse;
    }
}
