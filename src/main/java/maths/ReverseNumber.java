package maths;

public class ReverseNumber {
    public static int reverse(int x) {
        int previousReverse = 0;
        int reverse = 0;
        while ( x != 0){
            int digit = x % 10;
            x = x/10;
            reverse = (reverse * 10) + digit;
            // check to see if the number overflowed
            if((reverse - digit)/10 != previousReverse){
                return 0;
            }
            previousReverse = reverse;
        }
        return reverse;
    }

    public static void main(String[] args) {
//        reverse(-123);
        System.out.println(reverse(1534236469));
    }
}
