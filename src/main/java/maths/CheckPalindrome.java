package maths;

public class CheckPalindrome {
    public static boolean isPalindrome(int n){
        return n == ReverseNumber.reverse(n);
    }
}
