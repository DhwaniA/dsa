package string;

import java.util.HashMap;
import java.util.Map;

public class Anagram {
    public static boolean isAnagram(String s, String t) {
        Map<Character, Integer> charCount = new HashMap<>();
        // traverse first string
        for(int i = 0; i < s.length(); i++){
            char key = s.charAt(i);
            if(!charCount.containsKey(key)){
                charCount.put(key, 1);
            } else {
                Integer currentCount = charCount.get(key);
                charCount.put(key, currentCount +1);
            }
        }

        //traverse 2nd string
        for(int i = 0; i < t.length(); i++){
            char key = t.charAt(i);
            if(!charCount.containsKey(key)){
                return false;
            } else {
                Integer currentCount = charCount.get(key);
                charCount.put(key, currentCount -1);
            }
        }

        for(Integer val : charCount.values()){
            if(val != 0)
                return false;
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(isAnagram("rat","car"));
    }
}
