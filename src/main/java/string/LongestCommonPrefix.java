package string;

public class LongestCommonPrefix {
    public static String longestCommonPrefix(String[] strs) {
        if(strs.length == 1){
            return strs[0];
        }
        String initial = strs[0];
        for(int i = 1; i < strs.length; i++){
            initial = common(initial, strs[i]);
        }
        return initial;
    }

    public static String common(String a, String b){
        int length = Math.min(a.length(), b.length());
        int index = -1;
        for(int i = 0 ; i < length; i++){
            if(a.charAt(i) == b.charAt(i)){
                index = i;
            } else {
                index = i - 1;
                break;
            }
        }
        if(index == -1){
            return "";
        }
        return b.substring(0, index+1);
    }

    public static void main(String[] args) {
        String[] strs = {"flower","flow","flight"};
        System.out.println(LongestCommonPrefix.longestCommonPrefix(strs));
    }
}
