package string;

import java.util.HashMap;
import java.util.Map;

public class Isomorphic {
    public boolean isIsomorphic(String s, String t) {
        Map<Character, Character> mapping = new HashMap<>();
        if(s.length() != t.length())
            return false;

        for (int i = 0; i < s.length(); i++) {
            char sChar = s.charAt(i);
            char tChar = t.charAt(i);
            if(!mapping.containsKey(sChar)){
                if(mapping.containsValue(tChar))
                    return false;
                mapping.put(sChar, tChar);
            } else {
                if(mapping.get(sChar) != tChar){
                    return false;
                }
            }
        }
        return true;
    }
}
