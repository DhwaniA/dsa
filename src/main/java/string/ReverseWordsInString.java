package string;

import java.util.Stack;

public class ReverseWordsInString {
    public static String reverseWords(String s) {
        String trimedString = s.trim();
        String[] originalString = trimedString.split(" ");
        Stack<String> stack = new Stack<>();
        for(String word : originalString){
            if(!word.equals(""))
            stack.push(word);
        }
        StringBuffer sb = new StringBuffer();
        while (!stack.empty()){
            sb.append(stack.pop() + " ");
        }
        return sb.toString().substring(0, sb.length());
    }

    public static void main(String[] args) {
        System.out.println(ReverseWordsInString.reverseWords("a good   example"));
    }
}
