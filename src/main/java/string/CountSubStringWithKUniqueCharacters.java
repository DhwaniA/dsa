package string;

import java.util.HashMap;
import java.util.Map;

public class CountSubStringWithKUniqueCharacters {
    public static int countSubStrings(String str, int k) {
        // Write your code here.
        int ans = 0;
        for (int i = 0; i < str.length(); i++) {
            int distinct = 0;
            Map<Character, Integer> map = new HashMap<>();
            for (int j = i; j < str.length(); j++) {
                char curr = str.charAt(j);
                if(map.containsKey(curr)){
                    map.put(curr, map.get(curr) + 1);
                } else {
                    map.put(curr, 1);
                    distinct++;
                }
                if(distinct == k){
                    ans++;
                } else if (distinct > k){
                    continue;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        String s = "aacfssa";
        System.out.println(CountSubStringWithKUniqueCharacters.countSubStrings(
                s,3
        ));
    }
}
