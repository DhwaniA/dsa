package string;

public class RotateString {
    public boolean rotateString(String s, String goal) {
        if(s.length() != goal.length())
            return false;
        int shift = 0;
        int n = s.length();
        for (int i = 0; i < s.length(); i++) {
            if(i == 0){
                int positionInGoal = goal.indexOf(s.charAt(i));
                if(positionInGoal == -1)
                    return false;
                shift = (n - positionInGoal) % n;
            }
            int expectedPosition = ( i - shift + n) % n;
            if(goal.charAt(expectedPosition) != s.charAt(i)){
                return false;
            }
        }
        return true;
    }
}
