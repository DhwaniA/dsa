package binarysearch;

public class KoKoEatingBananas {
    public static int minEatingSpeed(int[] piles, int h) {
        // do bs on possible values of k
        int low = 0;
        int high = max(piles);
        while ( low < high){
            int mid = low + (high - low)/2;
            if(canFinish(piles, mid, h)){
                high = mid;
            } else {
                low = mid + 1;
            }
        }
        return low;
    }

    public static boolean canFinish(int[] piles, int speed, int h){
        int hrs = 0;
        for(int p: piles){
            hrs += Math.ceil((float) p / speed);
        }
        return hrs <= h;
    }

    public static int max(int[] piles){
        int max = Integer.MIN_VALUE;
        for(int p : piles){
            max = Math.max(max, p);
        }
        return max;
    }

    public static void main(String[] args) {
        int[] piles = {1000000000};
        System.out.println(minEatingSpeed(piles, 2));
    }
}
