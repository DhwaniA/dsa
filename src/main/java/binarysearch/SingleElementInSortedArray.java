package binarysearch;

public class SingleElementInSortedArray {
    static int singleNonDuplicate(int[] nums) {
        if(nums.length == 1){
            return nums[0];
        }
        int n = nums.length - 1;
        int low = 0, high = n;
        while(low <= high){
            int mid = low + (high - low)/2;
            if(mid > 0 && mid < n){
                if(nums[mid] != nums[mid - 1] && nums[mid] != nums[mid + 1]){
                    return nums[mid];
                } else if ( nums[mid] == nums[mid - 1]){
                    int left = mid - 0 - 1;
                    if ( left % 2 == 0){
                        low = mid + 1;
                    } else {
                        high = mid - 1;
                    }
                } else {
                    int right = n - mid - 1;
                    if( right % 2 == 0){
                        high = mid - 1;
                    } else {
                        low = mid + 1;
                    }
                }
            } else if ( mid == 0){
                if(nums[mid + 1] != nums[mid]){
                    return nums[mid];
                } else {
                    low = mid + 1;
                }
            } else if ( mid == n){
                if(nums[mid] != nums[mid - 1]){
                    return nums[mid];
                } else {
                    high = mid - 1;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,2,2,3};
        System.out.println(singleNonDuplicate(nums));
    }
}
