package binarysearch;

public class FindElementInRotatedSortedArray {
    public static int search(int[] nums, int target) {
        int pivot = NumberOfTimesArrayIsRotated.findKRotation(nums);
        if( pivot == 0){
            return BinarySearch.search(nums,target, 0, nums.length);
        } else {
            return Math.max(
                    BinarySearch.search(nums,target,0,pivot),
                    BinarySearch.search(nums,target,pivot,nums.length-1)
            );
        }
    }

    public static void main(String[] args) {
        int[] nums = {11,12,15,18,2,5,6,8};
        System.out.println(search(nums,6));
    }
}
