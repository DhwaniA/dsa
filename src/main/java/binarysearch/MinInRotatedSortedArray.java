package binarysearch;

public class MinInRotatedSortedArray {
    public static int search(int[] nums ) {
//        int low  = 0, high = nums.length -1;
        int low = 0;
        int high = nums.length - 1;
        while(low <= high){
            int mid = low + ((high - low)/2);
            //if the array is not rotated
            if(nums[low] <= nums[high]){
                return nums[low];
            }
            if(mid > 0 && mid < nums.length -1){
                if (nums[mid] <= nums[mid -1] && nums[mid] <= nums[mid+1]) {
                    return nums[mid];
                } else if(nums[mid] >= nums[low]){
                    low = mid + 1;
                }  else {
                    high = mid - 1;
                }
            } else if ( mid == 0 ){
                if(nums[mid] <= nums[mid + 1]){
                    return nums[mid];
                } else {
                    low = mid + 1;
                }
            } else if ( mid == nums.length -1){
                if(nums[mid] <= nums[mid -1]){
                    return nums[mid];
                } else {
                    high = mid -1;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] nums = {3,4,5,1,2};
        System.out.println(search(nums));
    }
}
