package binarysearch;

public class FirstAndLastOccurence {
    static int firstOccurence(int[] nums, int target) {
        int result = -1;
        int low  = 0, high = nums.length -1;
        while(low <= high){
            int mid = low + ((high - low)/2);
            if(nums[mid] > target){
                high = mid - 1;
            } else if (nums[mid] == target) {
                result = mid;
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return result;
    }

    static int lastOccurence(int[] nums, int target){
        int result = -1;
        int low  = 0, high = nums.length -1;
        while(low <= high){
            int mid = low + ((high - low)/2);
            if(nums[mid] > target){
                high = mid - 1;
            } else if (nums[mid] == target) {
               result = mid;
               low = mid +1;
            } else {
                low = mid + 1;
            }
        }
        return result;
    }
}
